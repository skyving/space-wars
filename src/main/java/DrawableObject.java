import java.awt.*;

public abstract class DrawableObject{
    int x = 0, y = 0, moveX = 0, moveY = 0;

    public abstract void paint(Graphics2D g2d);

    public abstract boolean update(boolean isPlaying);
}
