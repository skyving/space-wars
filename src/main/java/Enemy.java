import com.brashmonkey.spriter.Entity;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.awt.image.AffineTransformOp;
import java.util.Random;

public class Enemy extends com.brashmonkey.spriter.Player {

    int Health, damage;
    private boolean finished = false;
    private int hitboxWith, hitboxHiehgt, hitboxChangex, hitboxChangey;
    public int ScreenHeight;
    private boolean hasExploded;
    private Rectangle recEnemy;
    private int Speed = 1;
    private int randomShipNumber;
    private Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

    public Enemy(Entity entity, int Health, int x, int y, int damage, boolean hasExploded, int randomShipNumber){
        super(entity);
        this.randomShipNumber = randomShipNumber;
        if(!hasExploded){
            if(randomShipNumber == 0){setAnimation(0);}
            else if(randomShipNumber == 1){setAnimation(2);}
            else if(randomShipNumber == 2){setAnimation(5);}
            else if(randomShipNumber == 3){setAnimation(6);}
            else if(randomShipNumber == 4){setAnimation(8);}
            else if(randomShipNumber == 5){setAnimation(10);}
        }else {
            setAnimation(randomShipNumber);
            Speed = 0;
        }
        if ((int) (dim.getWidth() * 0.8) > 1600) {
            ScreenHeight = 900;
        }else{
            ScreenHeight = (int)(dim.getHeight()*0.8);
        }
        this.Health = Health;
        this.damage = damage;
        this.hasExploded = hasExploded;
        speed = 10;
        setPosition(x,y);
        flipX();
    }

    public void update() {
        hitbox();
        if((getTime() == getAnimation().length && hasExploded) || ((int)getX() + 100 < 0)){
            finished = true;
        }
        super.update();
        if(!finished){
            recEnemy = new Rectangle( (int)(getX() - getBoundingRectangle(null).size.width + hitboxChangex),ScreenHeight - (int)(getY()) - (int)getBoundingRectangle(null).size.height + hitboxHiehgt,(int)getBoundingRectangle(null).size.width - hitboxWith,(int)getBoundingRectangle(null).size.height - hitboxHiehgt - hitboxChangey);
        }
        setPosition(getX() - Speed,getY());
    }

    public boolean isExploded(){return hasExploded;}

    public boolean isFinished(){return finished;}

    public Rectangle getRecEnemy(){return recEnemy;}

    public int getRandomShipNumber(){return randomShipNumber;}

    public void hitbox(){
        if(getAnimation().id == 0){
            hitboxHiehgt = 50;
            hitboxWith = 20;
            hitboxChangex = 60;
        }
        else if(getAnimation().id == 2){
            hitboxHiehgt = 50;
            hitboxWith = 40;
            hitboxChangex = 65;
            hitboxChangey = 20;
        }
        else if(getAnimation().id ==5){
            hitboxHiehgt = 70;
            hitboxWith = 75;
            hitboxChangex = 60;
            hitboxChangey = 15;
        }
        else if(getAnimation().id ==6){
            hitboxHiehgt = 70;
            hitboxWith = 75;
            hitboxChangex = 60;
            hitboxChangey = 20;
        }
        else if(getAnimation().id == 8){
            hitboxHiehgt = 70;
            hitboxWith = 75;
            hitboxChangex = 60;
            hitboxChangey = 20;
        }
        else if(getAnimation().id == 10){
            hitboxHiehgt = 10;
            hitboxWith = 10;
            hitboxChangex = 30;
            hitboxChangey = 20;
        }
    }
}
