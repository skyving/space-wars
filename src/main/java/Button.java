import java.awt.*;
import java.awt.event.MouseEvent;

public class Button extends DrawableObject {

    public int x,y,textX,textY,width,height;
    public String text;
    private boolean isHovering;

    public Button(int x, int y, int textX, int textY, int width, int height, String text){
        this.x = x;
        this.y = y;
        this.textX = textX;
        this.textY = textY;
        this.width = width;
        this.height = height;
        this.text = text;
        isHovering = false;
    }

    public void paint(Graphics2D g2d){
        g2d.setColor(new Color(1,1,1,0.35f));
        if(isHovering)
            g2d.fillRect(x,y,width,height);
        g2d.setFont(new Font("Arial", Font.BOLD, 35));
        g2d.setColor(Color.WHITE);
        if(isHovering)
            g2d.setColor(Color.BLACK);
        g2d.drawString(text,textX,textY);
    }

    @Override
    public boolean update(boolean isPlaying) {
        return false;
    }

    public void mouseMovement(MouseEvent event) {
        isHovering = Game.isHovering(event,x,y,width,height);
    }

    public boolean mousePressed(MouseEvent event){
        return event.getX() >= x && event.getX() <= x + width && event.getY() >= y && event.getY() <= y + height;
    }
}
