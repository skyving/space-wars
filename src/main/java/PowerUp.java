import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class PowerUp {

    private int x, y;
    BufferedImage bi;
    Image image;
    boolean conect;

    public PowerUp(int x, int y, boolean conect){
        try {
            image = new ImageIcon(getClass().getResource("PowerUps/power.gif")).getImage();
            bi = ImageIO.read(getClass().getResource("PowerUps/power.gif"));
        }catch (IOException e){
            e.printStackTrace();
        }
        this.conect = conect;
        this.x = x - bi.getWidth() - 5;
        this.y = y + bi.getHeight()/2;
    }

    public void paint(Graphics2D g2d){
        if(conect){
            g2d.drawImage(image, x, y,bi.getWidth(),bi.getHeight(), null);
        }else{
            g2d.drawImage(image, x, y,bi.getWidth(),bi.getHeight(), null);
        }
    }

    public void update(){
        x-=3;
    }

}
