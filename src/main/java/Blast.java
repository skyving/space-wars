import com.brashmonkey.spriter.Entity;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.Console;
import java.io.IOException;

public class Blast extends com.brashmonkey.spriter.Player{

    private boolean hasExploded = false;
    private boolean finished = false;
    private int Speed = 0;
    public Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
    public double ScreenHeight;
    private double ScreenWitdh;

    private int randomShipNumber;
    private Rectangle hitbox;

    public Blast(Entity entity, int y, int x, boolean right, int randomShipNumber){
        super(entity);
        speed = 10;
        this.randomShipNumber = randomShipNumber;
        setPosition(x,y);
        if(randomShipNumber == 5){setAnimation(0);}
        else if(randomShipNumber == 4){setAnimation(2);}
        else if(randomShipNumber == 3){setAnimation(4);}
        else if(randomShipNumber == 2){setAnimation(6);}
        else if(randomShipNumber == 1){setAnimation(8);}
        else if(randomShipNumber == 0){setAnimation(10);};
        setScale(1.5f);
        if((int)(dim.getWidth()*0.8) > 1600){
            this.Speed = (int)(dim.getWidth()*0.8)/200;
            this.ScreenHeight = 900;
            this.ScreenWitdh = 1600;
        }else if((dim.getWidth()*0.8) < 1600){
            this.Speed = 1600/200;
            this.ScreenHeight = dim.getHeight()*0.8;
            this.ScreenWitdh = dim.getWidth()*0.8;
        }
        if(!right){
            Speed = -Speed;
            flipX();
        }
    }

    public boolean playerHit(){
        //return Game.playerIsHit((int)getX(),(int)getY());
        return false;
    }

    public void update() {
        if((getTime() == getAnimation().length && hasExploded) || ((getX() > ScreenWitdh && ScreenWitdh != 0)) || (getX() + 50 < 0)){
            finished = true;
        }
        super.update();
        HitBox();
        setPosition(getX() + Speed, getY());
    }

    public void explode(){
        setAnimation(getAnimation().id + 1);
        Speed = 0;
        hasExploded = true;
    }

    public boolean isFinished(){
        return finished;
    }

    public boolean getRight(){
        if(Speed > 0){return true;}
        else return false;
    }

    public void HitBox(){
        if(randomShipNumber == 0){
            hitbox = new Rectangle((int)getX() - (int)getBoundingRectangle(null).size.width + 170,(int)ScreenHeight - (int)getY() - (int)getBoundingRectangle(null).size.height + 120 ,(int)getBoundingRectangle(null).size.width - 170, (int)getBoundingRectangle(null).size.height - 170);
        }
        else if(randomShipNumber == 1){
            hitbox = new Rectangle((int)getX() - (int)getBoundingRectangle(null).size.width + 130,(int)ScreenHeight - (int)getY() - (int)getBoundingRectangle(null).size.height + 120 ,(int)getBoundingRectangle(null).size.width - 170, (int)getBoundingRectangle(null).size.height - 170);
        }
        else if(randomShipNumber == 2){
            hitbox = new Rectangle((int)getX() - (int)getBoundingRectangle(null).size.width + 60,(int)ScreenHeight - (int)getY() - (int)getBoundingRectangle(null).size.height + 35 ,(int)getBoundingRectangle(null).size.width - 80, (int)getBoundingRectangle(null).size.height - 60);
        }
        else if(randomShipNumber == 3){
            hitbox = new Rectangle((int)getX() - (int)getBoundingRectangle(null).size.width + 50,(int)ScreenHeight - (int)getY() - (int)getBoundingRectangle(null).size.height + 35,(int)getBoundingRectangle(null).size.width - 80, (int)getBoundingRectangle(null).size.height - 60);
        }
        else if(randomShipNumber == 4){
            hitbox = new Rectangle((int)getX() - (int)getBoundingRectangle(null).size.width + 70,(int)ScreenHeight - (int)getY() - (int)getBoundingRectangle(null).size.height + 40,(int)getBoundingRectangle(null).size.width - 80, (int)getBoundingRectangle(null).size.height - 70);
        }
        else if(randomShipNumber == 5){
            hitbox = new Rectangle((int)getX() - (int)getBoundingRectangle(null).size.width + 20,(int)ScreenHeight - (int)getY() - (int)getBoundingRectangle(null).size.height,(int)getBoundingRectangle(null).size.width - 30, (int)getBoundingRectangle(null).size.height- 35);
        }
    }

    public boolean HasExploded(){
        return hasExploded;
    }

    public Rectangle getHitbox(){
        return hitbox;
    }
}
