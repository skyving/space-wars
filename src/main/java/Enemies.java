import com.brashmonkey.spriter.Drawer;
import com.brashmonkey.spriter.Entity;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.PrimitiveIterator;
import java.util.Random;

public class Enemies {
    private Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
    private List<Enemy> enemies = new ArrayList<>();
    private List<Enemy> remove = new ArrayList<>();
    private int health = 100;
    private int damage = 10;
    private int round = 0;
    private int score = 0;
    private Graphics2D d;
    private int randomShipNumber;
    private Player player;
    private PowerUps powerUps;
    Entity entity;
    Entity explodedEntity;
    Blasts blasts;
    public Enemies(Entity entity, Entity entity1, Blasts blasts, Player player, PowerUps powerUps, int randomShipNumber){
        this.player= player;
        this.powerUps = powerUps;
        this.randomShipNumber = randomShipNumber;
        this.entity = entity;
        this.explodedEntity = entity1;
        this.blasts = blasts;
    }

    public void add(){
        round++;
       for(int x = 0; x < 1; x++){
           Random rand = new Random();
           enemies.add(new Enemy(entity,health,(int)dim.getWidth(),rand.nextInt( 400) + 100, damage, false, rand.nextInt(6)));
       }
    }

    public void update(Graphics2D d){
        this.d = d;
        shoot();
        for(Enemy enemy:remove){
            enemies.add(new Enemy(explodedEntity, enemy.Health,(int)enemy.getX(),(int)enemy.getY(),enemy.damage, true, enemy.getRandomShipNumber()));
            enemies.removeIf(n -> n.equals(enemy));
        }
        remove.clear();
        for(Enemy enemy:enemies){
            if(enemy.isFinished()){
                remove.add(enemy);
            }
        }
        for(Enemy enemy:remove){
            powerUps.add((int)enemy.getX(),
                     200, false);
            enemies.removeIf(n -> n.equals(enemy));
        }
        //enemy.getRecEnemy().y - enemy.getRecEnemy().width/2
        remove.clear();
        if(enemies.isEmpty()){add();}
        isHit();
    }

    public void isHit(){
        for(Enemy enemy:enemies){
            enemy.update();
            if(enemy.getEntity() == entity){
                if(enemy.getRecEnemy().intersects(player.getHitbox())){
                    remove.add(enemy);
                }
                else{
                    for(Blast blast:blasts.getBlasts()){
                        if(enemy.getRecEnemy().intersects(blast.getHitbox()) && blast.getRight()){
                            blast.explode();
                            remove.add(enemy);
                        }
                    }
                }
            }
        }
    }

    public void draw(Drawer drawer, Drawer drawer1){
        for(Enemy enemy:enemies){
           if(enemy.getEntity() == entity){ drawer.draw(enemy);}
           else if(enemy.getEntity() == explodedEntity) {drawer1.draw(enemy);}
        }
    }

    public int getScore(){return score;}

    public void shoot(){
        for(Enemy enemy:enemies){
            Random rand = new Random();
            if(rand.nextInt(300) == 23 && !enemy.isExploded()){
               blasts.addEnemyBlast((int)enemy.getY(),(int)enemy.getX() - (int)enemy.getBoundingRectangle(null).size.width/2 - 20,false,enemy.getRandomShipNumber());
            }
        }
    }

}
