import com.brashmonkey.spriter.Drawer;
import com.brashmonkey.spriter.Entity;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Blasts {
    private List <Blast> blastList = new ArrayList<>();
    private Entity entity;
    private int randomShipNumber;

    public Blasts(Entity entity, int randomShipNumber){
        this.entity = entity;
        this.randomShipNumber = randomShipNumber;
    }

    public void add(int y, int x, boolean right){
    blastList.add(new Blast(entity,y,x,right, randomShipNumber));
    }

    public void addEnemyBlast(int y, int x, boolean right, int ship){
        blastList.add(new Blast(entity,y,x,right, ship));
    }

    public void colide(){
       for(int x = 0; x < blastList.size() - 1; x++){
           for(int y = x+1; y < blastList.size(); y++){
               if(!blastList.get(x).HasExploded() && !blastList.get(y).HasExploded() && blastList.get(x).getHitbox().intersects(blastList.get(y).getHitbox())){
                   blastList.get(x).explode();
                   blastList.get(y).explode();
               }
           }
       }
    }

    public void update(){
        List<Blast> remove = new ArrayList<>();
        for(Blast blast:blastList){
            blast.update();
            if(blast.isFinished()){
                remove.add(blast);
            }
        }
        for(Blast blast:remove){
            blastList.removeIf(n -> n.equals(blast));
        }
        colide();
    }

    public void draw(Drawer drawer){
        for(Blast blast:blastList){
            drawer.draw(blast);
        }
    }

    public List<Blast> getBlasts(){
        return blastList;
    }
}
