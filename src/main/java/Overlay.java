import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

public class Overlay extends DrawableObject {

    private String Score = "0";
    private Button[] buttons;
    public boolean Playing = false;
    public int Playerhealth = 100;
    private int Screenhight = 0;
    private int Screenwidth = 0;
    public boolean Login = false;
    public Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

    public Overlay() {

        if((int)(dim.getWidth()*0.8)>1600){
            Screenwidth = 1600;
            Screenhight = 900;
        }else{
            Screenwidth = (int)(dim.getWidth()*0.8);
            Screenhight = (int)(dim.getHeight()*0.8);
        }

        buttons = new Button[]{
                new Button((int)(Screenwidth/2 - 125),(int)(Screenhight)/3,(int)(Screenwidth)/2 - 95,(int)(Screenhight)/3 + 50,250,75,"Start Offline"),
                new Button((int)(Screenwidth/2 - 125),(int)(Screenhight)/3 + 100,(int)(Screenwidth)/2 - 95,(int)(Screenhight)/3 + 150,250,75,"Start Online"),
                new Button((int)(Screenwidth/2 - 125),(int)(Screenhight)/3 + 200,(int)(Screenwidth)/2 - 45,(int)(Screenhight)/3 + 250,250,75,"Login")
        };
    }

    @Override
    public void paint(Graphics2D g2d) {
        g2d.setColor(Color.WHITE);
        g2d.setFont(new Font("Arial", Font.BOLD, 20));
        g2d.drawString(Score, Screenwidth - 100, 20);
        g2d.setFont(new Font("Arial", Font.BOLD, 45));
        if(!Playing){
            g2d.drawString("Space Wars", Screenwidth/2 - 125, 100);
            for (Button button: buttons) {
                button.paint(g2d);
            }
        }else{
            g2d.setColor(Color.RED);
            g2d.fillRect(Screenwidth/2 - 100,50,(int)(200*Playerhealth/100),25);
            g2d.setColor(Color.WHITE);
            g2d.drawRect(Screenwidth/2 - 100,50,200,25);
        }
    }

    public void setScore(int score){
        this.Score = "" + score;
    }
    @Override
    public boolean update(boolean isPlaying) {
        return false;
    }

    public void mouseMovement(MouseEvent event){
        for(Button button : buttons){
            button.mouseMovement(event);
        }
    }

    public void mousePressed(MouseEvent event){
       if(!Playing){
           int n = 0;
           for(Button button : buttons){
               n++;
               Playing = button.mousePressed(event);
               if (Playing && n != 3){
                    break;
               }
               else if(Playing){
                   Playing = false;
                   Login = true;
                   break;
               }
           }
       }
    }
}