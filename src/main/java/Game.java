import com.brashmonkey.spriter.Data;
import com.brashmonkey.spriter.SCMLReader;
import com.brashmonkey.spriter.java2d.Drawer;
import com.brashmonkey.spriter.java2d.Loader;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.AffineTransform;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Random;

public class Game extends JPanel {

    private static final AffineTransform identity = new AffineTransform();
    private Background bg;
    private Overlay overlay;
    private PowerUps powerUps;
    public static Player player;
    public Blasts blasts;
    private Enemies enemies;
    private Drawer playerDrawer, blastDrawer, explodedDrawer;
    private int height;
    private int randomShipNumber = 0;
    private boolean isPlaying = true;
    private Graphics2D d;

    public static boolean isHovering(MouseEvent event, int x, int y, int width, int height) {
        return event.getX() >= x && event.getX() <= x + width && event.getY() >= y && event.getY() <= y + height;
    }

    public Game(JFrame frame) {
        if(randomShipNumber == 0){
            Random rand = new Random();
            randomShipNumber = rand.nextInt(6);
        }
        bg = new Background();
        overlay = new Overlay();
        powerUps = new PowerUps();
        String scml = "";

        try{
            //read the files
            String[] tmp = {""};
            Files.readAllLines(Paths.get(getClass().getResource("PNG_Parts&Spriter_Animation/Shots.scml").toURI()))
                    .forEach(line -> tmp[0] += line);
            scml = tmp[0];
        }catch(IOException | URISyntaxException e){
            e.printStackTrace();
        }

        Data data = new SCMLReader(scml).getData();

        Loader loader = new Loader(data);
        loader.load("src/main/resources/PNG_Parts&Spriter_Animation");

        blastDrawer = new Drawer(frame,loader);

        blasts = new Blasts(data.getEntity(0), randomShipNumber);

        try{
            //read the files
            String[] tmp = {""};
            Files.readAllLines(Paths.get(getClass().getResource("PNG_Parts&Spriter_Animation/Flight+Ships.scml").toURI()))
                    .forEach(line -> tmp[0] += line);
            scml = tmp[0];
        }catch(IOException | URISyntaxException e){
            e.printStackTrace();
        }
        data = new SCMLReader(scml).getData();

        try{
            //read the files
            String[] tmp = {""};
            Files.readAllLines(Paths.get(getClass().getResource("PNG_Parts&Spriter_Animation/Explosions.scml").toURI()))
                    .forEach(line -> tmp[0] += line);
            scml = tmp[0];
        }catch(IOException | URISyntaxException e){
            e.printStackTrace();
        }
        Data data1 = new SCMLReader(scml).getData();
        player = new Player(data.getEntity(0), blasts, powerUps,randomShipNumber);
        enemies = new Enemies(data.getEntity(0), data1.getEntity(0),blasts, player, powerUps,randomShipNumber);

        Loader loaders = new Loader(data);
        loaders.load("src/main/resources/PNG_Parts&Spriter_Animation");
        Loader loader1 = new Loader(data1);
        loader1.load("src/main/resources/PNG_Parts&Spriter_Animation");

        playerDrawer = new Drawer(frame, loaders);
        explodedDrawer = new Drawer(frame,loader1);

        height = frame.getContentPane().getHeight();

        addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent keyEvent) {

            }

            @Override
            public void keyPressed(KeyEvent keyEvent) {
                player.keyPressed(keyEvent);
            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {
                player.keyReleased(keyEvent);
            }

        });

       addMouseMotionListener(new MouseMotionListener() {
           @Override
           public void mouseDragged(MouseEvent mouseEvent) {

           }

           @Override
           public void mouseMoved(MouseEvent mouseEvent) {
                overlay.mouseMovement(mouseEvent);
           }
       });

       addMouseListener(new MouseListener() {
           @Override
           public void mouseClicked(MouseEvent mouseEvent) {
                overlay.mousePressed(mouseEvent);
           }

           @Override
           public void mousePressed(MouseEvent mouseEvent) {

           }

           @Override
           public void mouseReleased(MouseEvent mouseEvent) {

           }

           @Override
           public void mouseEntered(MouseEvent mouseEvent) {

           }

           @Override
           public void mouseExited(MouseEvent mouseEvent) {

           }
       });

        setFocusable(true);
    }


    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON); //skicka till rasmus
        d = g2d;
        bg.paint(g2d);
        overlay.paint(g2d);
        powerUps.paint(g2d);
        playerDrawer.graphics = g2d;
        blastDrawer.graphics = g2d;
        explodedDrawer.graphics = g2d;
        if(player.getCurrentKey() != null){
            playerDrawer.draw(player);
            enemies.draw(playerDrawer, explodedDrawer);
            blasts.draw(blastDrawer);
            playerDrawer.graphics.setTransform(identity);
            playerDrawer.graphics.scale(1,-1);
            playerDrawer.graphics.translate(0,-height);
            blastDrawer.graphics.setTransform(identity);
            blastDrawer.graphics.scale(1,-1);
            blastDrawer.graphics.translate(0,-height);
        }
        overlay.paint(g2d);
    }

    public void update() {
        isPlaying = bg.update(isPlaying);
        isPlaying = player.update(isPlaying);
        isPlaying = overlay.update(isPlaying);
        overlay.setScore( enemies.getScore());
        enemies.update(d);
        blasts.update();
        powerUps.update();
        if(overlay.Login){
            try{
                Kominikation kom = new Kominikation();
                overlay.Login = false;
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        // Enable hardware acceleration
        System.setProperty("sun.java2d.opengl", "true"); // denna OCKSÅ
        JFrame frame = new JFrame();
        Game game = new Game(frame);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

        if ((int) (dim.getWidth() * 0.8) > 1600) {
            frame.setSize(1600, 900);
        }else{
            frame.setSize((int)(dim.getWidth()*0.8), (int)(dim.getHeight()*0.8));
        }

        frame.setLocation(dim.width/2-frame.getSize().width/2, dim.height/2-frame.getSize().height/2); //x y
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.add(game);
        frame.setTitle("Space Wars");
        frame.setResizable(false);
        frame.setVisible(true);
        UpdateThread thread = new UpdateThread(game);
        thread.start();
        while (true) {
            game.repaint();
        }
    }

}
