import com.sun.jdi.ThreadGroupReference;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLParameters;
import java.awt.*;
import java.io.IOException;
import java.net.*;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

public class Kominikation {
    public String username = "rasmus";
    public Kominikation() throws URISyntaxException, IOException, InterruptedException {

        //skapar en request till api:n
        HttpRequest request = HttpRequest.newBuilder().uri(new URI("http://localhost:3000/api/gamelogin")).GET().build();
        //skapar en client
        HttpClient client =  HttpClient.newHttpClient();
        //svaret från servern
        HttpResponse<String> authkey = client.send(request, HttpResponse.BodyHandlers.ofString());
        //respons.body = authkey
        if(Desktop.isDesktopSupported())
            Desktop.getDesktop().browse(new URI("http://localhost:3001/login?authkey=" + authkey.body()));

        request = HttpRequest.newBuilder().uri(new URI("http://localhost:3000/api/gamelogin/callback"))
                .header("content-type","application/json")
                .POST(HttpRequest.BodyPublishers.ofString("{\"authkey\": \"" + authkey.body() + "\"}"))
                .build();
        HttpResponse<String> tokens = client.send(request, HttpResponse.BodyHandlers.ofString());
        while(tokens.body() == ""){

        }
        System.out.println("Authkey " + authkey.body());
        System.out.println("Tokens " + tokens.body());

    }
}