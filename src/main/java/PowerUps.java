import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class PowerUps {
    List<PowerUp> powerUps = new ArrayList<>();

    public void add(int x, int y, boolean conect){
        Random rand = new Random();
        if(rand.nextInt(1) == 0){
           powerUps.add(new PowerUp(x,y, conect));
        }
    }

    public void update(){
        for(PowerUp powerUp:powerUps){
            powerUp.update();
        }
    }

    public void paint(Graphics2D g2d){
        for(PowerUp powerUp:powerUps){
            powerUp.paint(g2d);
        }
    }
}
