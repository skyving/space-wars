import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;

public class Background extends DrawableObject {

    BufferedImage bg;
    int x1, x2, x3;

    @SuppressWarnings("ConstantConditions")
    public Background() {
        x1 = 0;
        x2 = 900;
        x3 = 1800;
        moveX = 1;
        Random random = new Random();
        try {
            bg = ImageIO.read(getClass().getResource("Backgrounds/" + (random.nextInt(14) + 1) + ".png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void paint(Graphics2D g2d) {
        g2d.drawImage(bg, x1, 0,900,900,null);
        g2d.drawImage(bg, x2, 0,900,900,null);
        g2d.drawImage(bg, x3, 0,900,900,null);
    }

    @Override
    public boolean update(boolean isPlaying) {
        x1 -= moveX;
        x2 -= moveX;
        x3 -= moveX;

        if (x1 <= -900)
            x1 = x3 + 900;
        if (x2 <= -900)
            x2 = x1 + 900;
        if (x3 <= -900)
            x3 = x2 + 900;
        return true;
    }

}
