import com.brashmonkey.spriter.Entity;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Random;

public class Player extends com.brashmonkey.spriter.Player {

    public Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
    private int changey = 0;
    private int changex = 0;
    private Rectangle hitbox;
    public static int hitboxWith;
    public static int hitboxHiehgt;
    private int ScreenHeight = 0;
    public static int hx = 30;
    public static int hy;
    private long cooldown = 0L;
    private boolean canShoot = true;
    int moveY = 0;
    private Blasts blasts;

    @SuppressWarnings("ConstantConditions")
    public Player(Entity entity, Blasts blasts,PowerUps powerUps, int randomShipNumber) {
        super(entity);
        this.blasts = blasts;
        if(randomShipNumber == 5){setAnimation(10);}
        else if(randomShipNumber == 4){setAnimation(8);}
        else if(randomShipNumber == 3){setAnimation(6);}
        else if(randomShipNumber == 2){setAnimation(5);}
        else if(randomShipNumber == 1){setAnimation(2);}
        else if(randomShipNumber == 0){setAnimation(0);};
        setHitbox(randomShipNumber);
        hitbox = new Rectangle(hx+hitboxWith/2,   hy - (int)getBoundingRectangle(null).size.height + hitboxHiehgt,(int)getBoundingRectangle(null).size.width - hitboxWith - changex,(int)getBoundingRectangle(null).size.height - hitboxHiehgt - changey);
        if ((int) (dim.getWidth() * 0.8) > 1600) {
            setPosition(40,450);
            ScreenHeight = 900;
        }else{
            setPosition(40,(int)(dim.getHeight()*0.4) -(int)getBoundingRectangle(null).size.height/2);
            ScreenHeight = (int)(dim.getHeight()*0.8);
        }
    }

    public boolean update(boolean isPlaying) {
        super.update();
        hitbox = new Rectangle(hx+hitboxWith/2,   hy - (int)getBoundingRectangle(null).size.height + hitboxHiehgt,(int)getBoundingRectangle(null).size.width - hitboxWith - changex,(int)getBoundingRectangle(null).size.height - hitboxHiehgt - changey);
        setPosition(40,getY() - moveY);
        if((int)(getY()) < 5)
            setPosition(20,5);
        else if((int)(getY()) > 750)
            setPosition(20,750);
        hy = ScreenHeight - (int)(getY());
        if(!canShoot && System.currentTimeMillis() - cooldown >= 1000) canShoot = true;
        playerIsHit();
        return true;
    }

    public void playerIsHit(){
        for(Blast blast:blasts.getBlasts()){
            if(!blast.getRight() && hitbox.intersects(blast.getHitbox()) && !blast.HasExploded()){
                blast.explode();
            }
        }
    }

    public Rectangle getHitbox(){
        return hitbox;
    }

    public void keyPressed(KeyEvent event){
        if(event.getKeyCode() == KeyEvent.VK_UP || event.getKeyCode() == KeyEvent.VK_W){
            moveY = -3;
        }else if(event.getKeyCode() == KeyEvent.VK_DOWN || event.getKeyCode() == KeyEvent.VK_S){
            moveY = 3;
        }else if(event.getKeyCode() == KeyEvent.VK_SPACE && canShoot){
            blasts.add((int)getY(),(int)getX() + 120,true);
            canShoot = false;
            cooldown = System.currentTimeMillis();
        }

    }

    public void setHitbox(int randomShipNumber){
        if (randomShipNumber == 0) {
            hitboxHiehgt = 50;
            hitboxWith = 40;
            changex = 0;
        }else if(randomShipNumber == 1){
            hitboxHiehgt = 60;
            hitboxWith = 40;
            changey = 20;
            changex = 20;
        }
        else if(randomShipNumber == 2){
            hitboxHiehgt = 70;
            hitboxWith = 75;
            changey = 15;
        }
        else if(randomShipNumber == 3){
            hitboxHiehgt = 70;
            hitboxWith = 75;
            changey = 20;
        }
        else if(randomShipNumber == 4){
            hitboxHiehgt = 70;
            hitboxWith = 75;
            changey = 20;
        }
        else if(randomShipNumber == 5){
            hitboxHiehgt = 0;
            hitboxWith = 35;
            changey = 20;
        }
    }

    public void keyReleased(KeyEvent event){
        if(event.getKeyCode() == KeyEvent.VK_UP || event.getKeyCode() == KeyEvent.VK_W){
            moveY = 0;
        }else if(event.getKeyCode() == KeyEvent.VK_DOWN || event.getKeyCode() == KeyEvent.VK_S){
            moveY = 0;
        }
    }

}

