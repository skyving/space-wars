public class UpdateThread extends Thread {
    private Game game;
    public UpdateThread(Game game){
        this.game = game;
    }
    public void run(){
        while(true){
            game.update();
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
